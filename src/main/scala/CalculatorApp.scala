object CalculatorApp {

  class Calculator {
    def multiply(a: Int, b: Int): Int = a * b
    def add(a: Int, b: Int): Int = a + b
  }

}
