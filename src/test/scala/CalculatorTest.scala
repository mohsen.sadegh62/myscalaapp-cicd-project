import org.scalatest.funspec.AnyFunSpec

import CalculatorApp._

class CalculatorTest extends AnyFunSpec {
  val calculator = new Calculator

  describe("multiply") {
    it("should return 0 if multiply with any number") {
      assert(calculator.multiply(10, 0) == 0)
      assert(calculator.multiply(-10, 0) == 0)
    }
    it("should return 20 if 10 x 2") {
      assert(calculator.multiply(10, 2) == 20)
    }
  }

}
